# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.3.1](https://gitlab.com/cavilarts/poker-front/compare/v0.3.0...v0.3.1) (2020-04-27)


### Features

* creating coins component to manage the amount of chips that a player have ([256a9ad](https://gitlab.com/cavilarts/poker-front/commit/256a9ad6003463adc9f14ffb66aa9474293786b8))
* creating hand component [#3](https://gitlab.com/cavilarts/poker-front/issues/3) ([6b9a534](https://gitlab.com/cavilarts/poker-front/commit/6b9a534127ec0db02e842a1e2d57d5b059708d7b))
* table component with unit tests ([b4b57c7](https://gitlab.com/cavilarts/poker-front/commit/b4b57c77e10e165f9f3b9f052493d849491c8618))
* updatin unit tests and testing the seat component ([cb702ee](https://gitlab.com/cavilarts/poker-front/commit/cb702ee205130cf488a9ecff27227434a1307ee9))

## [0.3.0](https://gitlab.com/cavilarts/poker-front/compare/v0.2.0...v0.3.0) (2020-04-16)

## [0.2.0](https://gitlab.com/cavilarts/poker-front/compare/v0.1.1...v0.2.0) (2020-04-16)

### 0.1.1 (2020-04-16)
