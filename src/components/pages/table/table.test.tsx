import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';

import TablePage from './table';

describe('Table Page', () => {
  let table:ShallowWrapper;

  beforeEach(() => {
    table = shallow(<TablePage />);
  });

  it('should render the table page', () => {
    expect(table).toMatchSnapshot();
  });
});
