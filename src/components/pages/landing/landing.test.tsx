import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';

import LandingPage from './landing';

describe('Landing Page', () => {
  let landingPage:ShallowWrapper;

  beforeEach(() => {
    landingPage = shallow(<LandingPage />);
  });

  it('should render the landing page', () => {
    expect(landingPage).toMatchSnapshot();
  });
});
