import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import Table from './table';

describe('Table Component', () => {
  let table:ShallowWrapper;

  beforeEach(() => {
    table = shallow(<Table />);
  });

  it('should render the table component', () => {
    expect(table).toMatchSnapshot();
  });
});