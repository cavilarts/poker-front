import React, { ReactElement } from 'react';

// import { TableProps } from './table-interface';
import Seat from '../../molecules/seat/seat';
import './table.scss';

const Table:React.FC = (props) => {
  return (
    <section className="table-container">
      <div className="table">
        {renderSeats()}
      </div>
    </section>
  );
};

function renderSeats():ReactElement {
  return (
    <>
      <div className="table--seat-one">
        <Seat isEmpty={true} playerHand={[]} />
      </div>
      <div className="table--seat-two">
        <Seat isEmpty={true} playerHand={[]} />
      </div>
      <div className="table--seat-three">
        <Seat isEmpty={true} playerHand={[]} />
      </div>
      <div className="table--seat-four">
        <Seat isEmpty={true} playerHand={[]} />
      </div>
      <div className="table--seat-five">
        <Seat isEmpty={true} playerHand={[]} />
      </div>
      <div className="table--seat-six">
        <Seat isEmpty={true} playerHand={[]} />
      </div>
      <div className="table--seat-seven">
        <Seat isEmpty={true} playerHand={[]} />
      </div>
      <div className="table--seat-eight">
        <Seat isEmpty={true} playerHand={[]} />
      </div>
    </>
  );
}

export default Table;