import React from 'react';

import PlayerInfo from '../../molecules/player-info/player-info';
import Info from '../../molecules/info/info';
import { NavigationProps } from './navigation-interface';
import './navigation.scss';

const Navigation:React.FC<NavigationProps> = (props) => {
  const { user } = props;

  return (
    <nav className="navigation">
      {user && user.userName ? <PlayerInfo userName={user.userName} userIcon={user.userIcon} /> : null }
      <div className="navigation--info">
        <Info />
      </div>
    </nav>
  );
}

export default Navigation;
