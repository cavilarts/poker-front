import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';

import Navigation from './navigation';
import PlayerInfo from '../../molecules/player-info/player-info';
import Info from '../../molecules/info/info';

describe('Navigation Component', () => {
  let navigation:ShallowWrapper;

  beforeEach(() => {
    navigation = shallow(<Navigation user={{
      userName: 'Player 1',
      userIcon: 'mustache'
    }} />);
  });

  it('should render a navigation component', () => {
    expect(navigation).toMatchSnapshot();
  });

  it('should contain a PlayerInfo component', () => {
    expect(navigation.find(PlayerInfo)).toHaveLength(1);
  });

  it('should contain a Info component', () => {
    expect(navigation.find(Info)).toHaveLength(1);
  });

  it('should not render PlayerInfo if no player data', () => {
    navigation.setProps({
      user: undefined
    });
    expect(navigation.find(PlayerInfo)).toHaveLength(0);
  });
});
