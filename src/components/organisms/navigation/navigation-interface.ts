import { PlayerInfoProps } from '../../molecules/player-info/player-info-interface';

export interface NavigationProps {
  user?: PlayerInfoProps
}