import React from 'react';

import Navigation from '../../organisms/navigation/navigation';

const TableTemplate:React.FC = () => {
  return (
    <section>
      Table
      <Navigation user={ {userName:'Carlos Avila', userIcon:'diamonds'} } />
    </section>
  );
};

export default TableTemplate;
