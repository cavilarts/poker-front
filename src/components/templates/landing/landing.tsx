import React from 'react';

import './landing.scss';
import { Link } from 'react-router-dom';

const LandingTemplate:React.FC = () => {
  return (
    <section className="landing-page">
      <nav className="landing-page--nav">
        <div className="nav--player-icon"></div>
        <div className="nav--player-data">
          <div className="player-name"></div>
          <div className="player-chips"></div>
        </div>
        <div className="nav--player-about"></div>
      </nav>
      <section className="landing-page--dashboard">
        <div className="landing-page--player">
          <div className="player--heading">
            <div className="player--name"></div>
            <div className="player--icon"></div>
            <div className="player--chips">
              <div className="player--chips-amount"></div>
              <div className="player--chips-get-more"></div>
            </div>
          </div>
          <div className="player--stats"></div>
        </div>
        <div className="landing-page--rooms">
          <div className="rooms--add"></div>
          <ul className="rooms--list">
            <li className="room--list-room">
              <Link className="room--link" to="/table">
                <div className="room--name"></div>
                <div className="room--num-players"></div>
                <div className="room--enter"></div>
              </Link>
            </li>
            <li className="room--list-room">
              <Link className="room--link" to="/table">
                <div className="room--name"></div>
                <div className="room--num-players"></div>
                <div className="room--enter"></div>
              </Link>
            </li>
            <li className="room--list-room">
              <Link className="room--link" to="/table">
                <div className="room--name"></div>
                <div className="room--num-players"></div>
                <div className="room--enter"></div>
              </Link>
            </li>
            <li className="room--list-room">
              <Link className="room--link" to="/table">
                <div className="room--name"></div>
                <div className="room--num-players"></div>
                <div className="room--enter"></div>
              </Link>
            </li>
            <li className="room--list-room">
              <Link className="room--link" to="/table">
                <div className="room--name"></div>
                <div className="room--num-players"></div>
                <div className="room--enter"></div>
              </Link>
            </li>
            <li className="room--list-room">
              <Link className="room--link" to="/table">
                <div className="room--name"></div>
                <div className="room--num-players"></div>
                <div className="room--enter"></div>
              </Link>
            </li>
          </ul>
        </div>
      </section>
    </section>
  );
};

export default LandingTemplate;