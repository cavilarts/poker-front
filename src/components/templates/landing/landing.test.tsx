import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';

import LandingTemplate from './landing';

describe('Landing Template', () => {
  let landingTemplate:ShallowWrapper;

  beforeEach(() => {
    landingTemplate = shallow(<LandingTemplate />);
  });

  it('should render de landing template layout', () => {
    expect(landingTemplate).toMatchSnapshot();
  });
});
