import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';

import Info from './info';
import Icon from '../../atoms/icon/icon';

describe('Info component', () => {
  let info:ShallowWrapper;

  beforeEach(() => {
    info = shallow(<Info />);
  });

  it('should render an info component', () => {
    expect(info).toMatchSnapshot();
  });

  it('should have a info-overlay class by default', () => {
    expect(info.find('.info-overlay')).toHaveLength(1);
  });

  it('should should show the overlay on button click', () => {
    info.find('.info-button').simulate('click');
    expect(info.find('.info-overlay.active')).toHaveLength(1);
  });

  it('should contain an Icon Component', () => {
    expect(info.find(Icon)).toHaveLength(1);
    expect(info.find(Icon).props()).toStrictEqual({
      name: 'wondering',
      size: 'small'
    });
  });
});
