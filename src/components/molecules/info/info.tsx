import React, { useState } from 'react';

import Icon from '../../atoms/icon/icon';
import './info.scss';

const Info:React.FC = () => {
  const [activeOverlay, setActiveOverlay] = useState(false);
  const overlayClass = activeOverlay ? 'info-overlay active' : 'info-overlay';

  return (
    <div className="info">
      <button className="info-button" onClick={() => { setActiveOverlay(!activeOverlay) }}>
        <Icon name="wondering" size="small" />
      </button>
      <div className={overlayClass}>
        <p className="info-overlay--text">Made with <span className="icon-heart"></span> by <a href="https://gitlab.com/cavilarts">@Cavilarts</a></p>
      </div>
    </div>
  );
}

export default Info;