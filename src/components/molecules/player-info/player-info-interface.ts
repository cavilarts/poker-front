import { IconProps } from '../../atoms/icon/icon-interface';

export interface PlayerInfoProps {
  userName: string;
  userIcon: IconProps['name']
}