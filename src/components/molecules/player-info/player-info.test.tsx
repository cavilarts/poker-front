import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';

import Icon from '../../atoms/icon/icon';
import PlayerInfo from './player-info';

describe('Player Info Component', () => {
  let playerInfo:ShallowWrapper;

  beforeEach(() => {
    playerInfo = shallow(<PlayerInfo userName="Player" userIcon="clubs" />);
  });

  it('should render the component', () => {
    expect(playerInfo).toMatchSnapshot();
  });

  it('should contain an Icon component', () => {
    expect(playerInfo.find(Icon)).toHaveLength(1);
    expect(playerInfo.find(Icon).props()).toStrictEqual({
      name: 'clubs',
      size: 'small'
    });
  });

  it('should contain a user name element', () => {
    expect(playerInfo.find('.player-info--user-name')).toHaveLength(1);
    expect(playerInfo.find('.player-info--user-name').props()).toStrictEqual({
      children: 'Player',
      className: 'player-info--user-name',
    });
  });
});
