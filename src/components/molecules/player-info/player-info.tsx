import React from 'react';

import Icon from '../../atoms/icon/icon';
import { PlayerInfoProps } from './player-info-interface';
import './player-info.scss';

const PlayerInfo:React.FC<PlayerInfoProps> = (props) => {
  const { userName, userIcon } = props;

  return (
    <section className="player-info">
      <Icon name={userIcon} size="small" />
      <p className="player-info--user-name">{userName}</p>
    </section>
  );
}

export default PlayerInfo;
