import React, { ReactNode } from 'react';

import Coins from '../../atoms/coins/coins';
import Hand from '../../atoms/hand/hand';
import { CardProps } from '../../atoms/card/card-interface';
import { SeatProps } from './seat-interface';
import './seat.scss';

const Seat:React.FC<SeatProps> = (props) => {
  const { isEmpty, playerName, coins, playerHand } = props;

  return (
    <div className="seat">
      {isEmpty ? renderEmptySeat() : renderPlayerSeat(playerName, coins, playerHand)}
    </div>
  );
};

function renderEmptySeat():ReactNode {
  return (
    <div className="seat--place seat--place__empty">
      Seat
    </div>
  );
};

function renderPlayerSeat(playerName:string | undefined, coins?:number, playerHand?:CardProps['name'][]):ReactNode {
  return (
    <div className="seat--place__player">
      <div className="seat--player-container">
        {playerHand ? renderHand(playerHand): null}
        <div className="seat--place seat--place__player-face">
          <p className="seat--place__player-name">{playerName}</p>
          {renderCoins(coins)}
        </div>
      </div>
    </div>
  );
}

function renderCoins(coins?:number) {
  return <Coins amount={coins || 0} />
}

function renderHand(hand:CardProps['name'][]):ReactNode {
  return <Hand cards={hand} />;
}

export default Seat;
