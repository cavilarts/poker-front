import { CardProps } from '../../atoms/card/card-interface';

export interface SeatProps {
  isEmpty: boolean;
  playerName?: string | undefined;
  playerHand: CardProps['name'][];
  coins?: number | undefined;
}