import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';

import Hand from '../../atoms/hand/hand';
import Coins from '../../atoms/coins/coins';
import Seat from './seat';

describe('Seat Component', () => {
  let seat:ShallowWrapper;
  
  beforeEach(() => {
    seat = shallow(<Seat isEmpty={false} playerName="Carlos" playerHand={['AS', 'KH']} coins={2000} />);
  });

  it('should render a seat with a player', () => {
    expect(seat).toMatchSnapshot();
  });

  it('should have a `Coins` component', () => {
    expect(seat.find(Coins)).toHaveLength(1);
  });

  it('should have a hand component', () => {
    expect(seat.find(Hand)).toHaveLength(1);
  });

  it('should have a playername', () => {
    expect(seat.find('.seat--place__player-name')).toHaveLength(1);
    expect(seat.find('.seat--place__player-name').text()).toBe('Carlos');
  });

  it('should render an empty hand theres no values in `playerHand`', () => {
    seat.setProps({
      playerHand: []
    });
    expect(seat.find(Hand)).toHaveLength(1);
    expect(seat.find(Hand).props()).toStrictEqual({cards: []});
  });

  it('the hand does not come it should not render a hand', () => {
    seat.setProps({
      playerHand: undefined
    });
    expect(seat.find(Hand)).toHaveLength(0);
  })

  it('should render 0 coins if there is a player with no coins', () => {
    seat.setProps({
      coins: undefined
    });
    expect(seat.find(Coins).length).toBe(1);
    expect(seat.find(Coins).props()).toStrictEqual({amount: 0});
  });

  describe('when is an available seat', () => {
    beforeEach(() => {
      seat.setProps({
        isEmpty: true,
        playerHand:[]
      });
    });

    it('should render an empty seat component', () => {
      expect(seat).toMatchSnapshot();
    });

    it('should not render the coins component', () => {
      expect(seat.find(Coins)).toHaveLength(0);
    })

    it('should not render the hand component', () => {
      expect(seat.find(Hand)).toHaveLength(0);
    });
  });
});