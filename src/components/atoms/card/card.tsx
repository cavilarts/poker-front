import React from 'react';

import { CardProps } from './card-interface';

import './card.scss';

const Card:React.FC<CardProps> = (props) => {
  const { name } = props;
  const cardPath = `/images/${name}.svg`;

  return(
    <img className="card" src={cardPath} alt={name} />
  );
};

export default Card;