import React from 'react';
import { shallow } from 'enzyme';

import Card from './card';

describe('Card Component', () => {
  it('should work', () => {
    const card = shallow(<Card name="AS" />)
    expect(card).toMatchSnapshot();
  });
});