export interface IconProps {
  name: 'pacman' | 'spades' | 'clubs' | 'diamonds' |
    'mug' | 'shield' | 'heart' | 'angry' |
    'sleepy' | 'wondering' | 'mustache' |
    'neutral' | 'confused' | 'baffled' |
    'shocked1' | 'evil' | 'angry1' | 'cool' |
    'grin' | 'wink' | 'sad' | 'tongue' | 
    'smile' | 'happy' | 'gamepad' | 'dollar' |
    'safe' | 'radioactive' | 'rocket'
  size: 'small' | 'medium' | 'large';
}