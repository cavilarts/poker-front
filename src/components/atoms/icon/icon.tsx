import React from 'react';

import { IconProps } from './icon-interface';
import './icon.scss';

const Icon:React.FC<IconProps> = (props) => {
  const { name, size } = props;
  const className = `icon icon-${name} icon_${size}`;

  return(
    <div className={className}></div>
  );
};

export default Icon;