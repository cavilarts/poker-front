import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';

import Icon from './icon';

describe('Icon Component', () => {
  let icon:ShallowWrapper;

  beforeEach(() => {
    icon = shallow(<Icon name="spades" size="small" />);
  });

  it('should render an small icon', () => {
    expect(icon).toMatchSnapshot();
    expect(icon.props()).toStrictEqual({
      className: 'icon icon-spades icon_small'
    });
  });

  it('should render a medium icon', () => {
    icon.setProps({
      size: 'medium'
    });
    expect(icon).toMatchSnapshot();
    expect(icon.props()).toStrictEqual({
      className: 'icon icon-spades icon_medium'
    });
  });

  it('should render a medium icon', () => {
    icon.setProps({
      size: 'large'
    });
    expect(icon).toMatchSnapshot();
    expect(icon.props()).toStrictEqual({
      className: 'icon icon-spades icon_large'
    });
  });
});