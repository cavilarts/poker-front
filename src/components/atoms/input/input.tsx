import React from 'react';

import { InputProps } from './input-interface';

const Input:React.FC<InputProps> = (props) => {
  const { type, disabled, autofocus, maxlength, placeholder, value } = props;
  const inputClass = `input--${type}`;

  return <input
    className={inputClass}
    type={type}
    value={value}
    disabled={disabled}
    autoFocus={autofocus}
    maxLength={maxlength}
    placeholder={placeholder}  
  />
};

export default Input;
