import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';

import Input from './input';

describe('Input Component', () => {
  let input:ShallowWrapper;

  beforeEach(() => {
    input = shallow(<Input type='button' value='I am a button' />);
  });

  it('should render a button', () => {
    expect(input).toMatchSnapshot();
    expect(input.props()).toStrictEqual({
      type: 'button',
      value: 'I am a button',
      autoFocus: undefined,
      className: "input--button",
      disabled: undefined,
      maxLength: undefined,
      placeholder: undefined,
    });
  });

  it('should render a checkbox', () => {
    input.setProps({
      type: 'checkbox',
      value: 'check',
    });
    expect(input.props()).toStrictEqual({
      type: 'checkbox',
      value: 'check',
      autoFocus: undefined,
      className: "input--checkbox",
      disabled: undefined,
      maxLength: undefined,
      placeholder: undefined,
    });
  });
});
