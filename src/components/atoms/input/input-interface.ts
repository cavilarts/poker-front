export interface InputProps {
  type: 'button' | 'checkbox' | 'number' | 'submit' | 'text';
  disabled?: boolean;
  autofocus?: boolean;
  maxlength?: number;
  placeholder?: string;
  value?: string;
}