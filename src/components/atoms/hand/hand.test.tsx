import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';

import Card from '../card/card';
import Hand from './hand';
import { EXDEV } from 'constants';

describe('Hand Component', () => {
  let hand:ShallowWrapper;

  beforeEach(() => {
    hand = shallow(<Hand cards={['AS', 'KH']}  />);
  });

  it('should render the component', () => {
    expect(hand).toMatchSnapshot();
  });

  it('should contain two `Cards`', () => {
    expect(hand.find(Card).length).toBe(2);
  });

  it('should have a hand wrapper', () => {
    expect(hand.find('.hand').length).toBe(1);
  });

  describe('when the comonent doesnt receive cards', () => {
    beforeEach(() => {
      hand.setProps({
        cards: []
      });
    });

    it('should render the empty hand', () => {
      expect(hand).toMatchSnapshot();
    });

    it('should render the empty cards wrapper', () => {
      expect(hand.find('.hand--empty').length).toBe(1);
    });

    it('should render two hand slots', () => {
      expect(hand.find('.hand--solt').length).toBe(2);
    });
  });
});