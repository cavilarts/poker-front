import { CardProps } from '../card/card-interface';

export interface HandProps {
  cards: CardProps['name'][];
}