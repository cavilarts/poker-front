import React, { ReactElement } from 'react';

import Card from '../card/card';
import { HandProps } from './hand-interface';
import { CardProps } from '../card/card-interface'

const Hand:React.FC<HandProps> = (props) => {
  const { cards } = props;

  return (
    <div className="hand">
      {cards.length ? cards.map(renderCard) : renderEmptyHand()}
    </div>
  );
};

function renderEmptyHand():ReactElement {
  return (
    <div className="hand--empty">
      <div className="hand--solt"></div>
      <div className="hand--solt"></div>
    </div>
  );
}

function renderCard(card:CardProps['name'], i:number):ReactElement {
  return <Card name={card} key={i} />;
}

export default Hand;
