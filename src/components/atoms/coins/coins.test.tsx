import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';

import Coins from './coins';

describe('Coins Component', () => {
  let coins:ShallowWrapper;

  beforeEach(() => {
    coins = shallow(<Coins amount={9999} />)
  });

  it('should render a coins component', () => {
    expect(coins).toMatchSnapshot();
  });

  it('should contain a prop `amount` with its corresponding value', () => {
    expect(coins.prop('children')).toBe(9999);
  });

  it('should update the `amount` value when is updated via props', () => {
    coins.setProps({amount: 3000});
    expect(coins.prop('children')).toBe(3000);
  });
});
