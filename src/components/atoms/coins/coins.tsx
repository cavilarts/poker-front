import React from 'react';

import { CoinsProps } from './coins-interface';
import './coins.scss';

const Coins: React.FC<CoinsProps> = (props) => {
  const { amount } = props;

  return (
    <div className="coins">{amount}</div>
  );
}

export default Coins;