import React from 'react';
import { Switch, Route } from 'react-router-dom';

import TableTemplate from './components/templates/table/table';
import LandingTemplate from './components/templates/landing/landing';

import './App.scss';

function App() {
  return (
    <main>
      <Switch>
        <Route path="/" component={LandingTemplate} exact/>
        <Route path="/table" component={TableTemplate} />
      </Switch>
    </main>
  );
}

export default App;
